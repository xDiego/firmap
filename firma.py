import lexer_rules
import parser_rules
import ply.lex as lex
lexer = lex.lex(module=lexer_rules)
import ply.yacc as yacc
parser = yacc.yacc(module=parser_rules)

while True:
    try:
        s = input('--> ')
    except EOFError | s == 'exit;':
        break
    parser.parse(s)
# lexer.input(input('--> '))
# while True:
#     tok = lexer.token()
#     if not tok:
#         break      # No more input
#     print(tok)
