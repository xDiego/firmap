from lexer_rules import *
# Parsing rules
cont = []
contf = []
par = []
trazos = {'firma':0}
precedence = ()

# dictionary of names
names = {}

def p_inicializacion_init(t):
    '''estructure   : KDATA ID SEMICOLON
                    | KDATA ID EQUAL ID SEMICOLON
                    | KDATA ID EQUAL ID DOT ID SEMICOLON'''

    arr = [t[2],t[1]]
    cont.append(arr)
    trazos['firma'] = trazos['firma'] + 1

def p_inicializacion_var(t):
    '''var      : ID
                | ID COMMA var
                | ID EQUAL ID
                | ID EQUAL ID COMMA var
                | ID EQUAL ID DOT ID
                | ID EQUAL ID DOT ID COMMA var
                | ID LPAREN var RPAREN
                | ID LPAREN var RPAREN COMMA var'''

def p_asignacion_var(t):
    '''estructure       : ID EQUAL ID SEMICOLON
                        | ID EQUAL ID LPAREN var RPAREN SEMICOLON
                        | ID EQUAL ID PLUS ID SEMICOLON
                        | ID EQUAL ID MINUS ID SEMICOLON
                        | ID EQUAL ID TIMES ID SEMICOLON
                        | ID EQUAL ID DIVIDE ID SEMICOLON
                        | ID EQUAL ID DOT ID SEMICOLON
                        | ID EQUAL ID DOT ID PLUS ID DOT ID SEMICOLON
                        | ID EQUAL ID DOT ID MINUS ID DOT ID SEMICOLON
                        | ID EQUAL ID DOT ID TIMES ID DOT ID SEMICOLON
                        | ID EQUAL ID DOT ID DIVIDE ID DOT ID SEMICOLON '''
    trazos['firma'] = trazos['firma'] + 1

def p_funciones_init(t):
    '''estructure       : KDATA ID LPAREN params RPAREN LKEY inside RKEY
                        | KDATA ID LPAREN params RPAREN LKEY inside RETURN ID SEMICOLON RKEY
                        | KDATA ID LPAREN params RPAREN SEMICOLON
                        | KDATA ID LPAREN RPAREN LKEY inside RKEY
                        | KDATA ID LPAREN RPAREN LKEY inside RETURN ID SEMICOLON RKEY
                        | KDATA ID LPAREN RPAREN SEMICOLON'''
    global par
    arr = [t[2],t[1],par]
    contf.append(arr)
    par = []
    trazos['firma'] = trazos['firma'] + 2

def p_funciones_params(t):
    '''params       :
                    | KDATA ID
                    | KDATA ID COMMA params'''
    par.append(t[1])

def p_funciones_call(t):
    'estructure      : ID LPAREN var RPAREN SEMICOLON'
    trazos['firma'] = trazos['firma'] + 2

def p_inside(t):
    '''inside      : estructure
                    | estructure inside'''

def p_controlestructure_if(t):
    '''estructure       : IF LPAREN condicion RPAREN LKEY inside RKEY
                        | IF LPAREN condicion RPAREN LKEY inside RKEY ELSE LKEY inside RKEY'''
    trazos['firma'] = trazos['firma'] + 3

def p_controlestructure_condition(t):
    '''condicion    : ID
                    | ID SAME ID
                    | ID GREATERT ID
                    | ID GEQT ID
                    | ID LEQT ID
                    | ID LESST ID
                    | ID GREATERT ID AND condicion
                    | ID GEQT ID OR condicion
                    | ID LEQT ID AND condicion
                    | ID LESST ID OR condicion
                    | ID SAME ID AND condicion
                    | ID SAME ID OR condicion
                    | ID MOD ID
                    | ID MOD ID condicion
                    | ID MOD ID SAME ID
                    | ID MOD ID SAME ID condicion'''

def p_controlestructure_while(t):
    'estructure      : WHILE LPAREN condicion RPAREN LKEY inside RKEY'
    trazos['firma'] = trazos['firma'] + 4

def p_controlestructure_do(t):
    'estructure      : DO LKEY inside RKEY WHILE LPAREN condicion RPAREN SEMICOLON'
    trazos['firma'] = trazos['firma'] + 4

def p_controlestructure_switch(t):
    'estructure      : SWITCH LPAREN ID RPAREN LKEY switchcase RKEY'
    trazos['firma'] = trazos['firma'] + 5

def p_controlestructure_case(t):
    '''switchcase       : CASE ID TDOT LKEY inside BREAK SEMICOLON RKEY
                        | CASE ID TDOT inside BREAK SEMICOLON
                        | DEFAULT TDOT LKEY inside BREAK SEMICOLON RKEY
                        | DEFAULT TDOT inside BREAK SEMICOLON
                        | CASE ID TDOT LKEY inside BREAK SEMICOLON RKEY switchcase
                        | CASE ID TDOT inside BREAK SEMICOLON switchcase
                        | DEFAULT TDOT LKEY inside BREAK SEMICOLON RKEY switchcase
                        | DEFAULT TDOT inside BREAK SEMICOLON switchcase'''
    trazos['firma'] = trazos['firma'] + 3

def p_controlestructure_for(t):
    'estructure      : FOR LPAREN var SEMICOLON condicion SEMICOLON operation RPAREN LKEY inside RKEY'
    trazos['firma'] = trazos['firma'] + 4

def p_operation(t):
    '''operation    : ID EQUAL ID PLUS ID
                    | ID EQUAL ID MINUS ID
                    | ID EQUAL ID TIMES ID
                    | ID EQUAL ID DIVIDE ID
                    | ID INCREMENT
                    | ID DECREMENT'''

def p_finish(t):
    'estructure      : EXIT SEMICOLON'
    print ("VARIABLES")
    for i in range(len(cont)):
        for j in range(len(cont[i])):
            print(cont[i][j], end=' ')
        print()
    print()
    print ("FUNCIONES")
    for i in range(len(contf)):
        for j in range(len(contf[i])):
            print(contf[i][j], end=' ')
        print()
    print()
    print ("FIRMA: '%s'" % trazos['firma'])

def p_error(t):
    print("Syntax error at '%s'" % t.value)
