reserved = {
   'if' : 'IF',
   'else' : 'ELSE',
   'for' : 'FOR',
   'while' : 'WHILE',
   'do' : 'DO',
   'return' : 'RETURN',
   'switch' : 'SWITCH',
   'case' : 'CASE',
   'break' : 'BREAK',
   'default' : 'DEFAULT',
   'int' : 'KDATA',
   'float' : 'KDATA',
   'void' : 'KDATA',
   'bool' : 'KDATA'
}
# Tokens
tokens = (
   'PLUS',
   'MINUS',
   'TIMES',
   'DIVIDE',
   'LPAREN',
   'RPAREN',
   'LKEY',
   'RKEY',
   'TDOT',
   'COMMA',
   'DOT',
   'SEMICOLON',
   'ID',
   'AND',
   'OR',
   'LEQT',
   'GEQT',
   'SAME',
   'EQUAL',
   'DECREMENT',
   'INCREMENT',
   'LESST',
   'GREATERT',
   'EXIT',
   'MOD'
) + tuple(reserved.values())

# Regular expression rules for simple tokens
t_PLUS          = r'\+'
t_INCREMENT     = r'\+\+'
t_MINUS         = r'\-'
t_DECREMENT     = r'\-\-'
t_TIMES         = r'\*'
t_DIVIDE        = r'\/'
t_EQUAL         = r'\='
t_SAME          = r'\=\='
t_LESST         = r'\<'
t_LEQT          = r'\<\='
t_GREATERT      = r'\>'
t_GEQT          = r'\>\='
t_AND           = r'\&\&'
t_OR            = r'\|\|'
t_LPAREN        = r'\('
t_RPAREN        = r'\)'
t_LKEY          = r'\{'
t_RKEY          = r'\}'
t_SEMICOLON     = r'\;'
t_TDOT          = r'\:'
t_COMMA         = r'\,'
t_DOT           = r'\.'
t_MOD           = r'\%'

def t_EXIT(t):
    r'exit+'
    t.type = reserved.get(t.value,'EXIT')    # Check for reserved words
    return t

def t_ID(t):
    r'[a-zA-Z0-9\"\,\%\\\&\*\:]+'
    t.type = reserved.get(t.value,'ID')    # Check for reserved words
    return t

def t_KDATA(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value,'KDATA')    # Check for reserved words
    return t

def t_FOR(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value,'FOR')    # Check for reserved words
    return t

def t_IF(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value,'IF')    # Check for reserved words
    return t

def t_ELSE(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value,'ELSE')    # Check for reserved words
    return t

def t_SWITCH(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value,'SWITCH')    # Check for reserved words
    return t

def t_DEFAULT(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value,'DEFAULT')    # Check for reserved words
    return t

def t_CASE(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value,'CASE')    # Check for reserved words
    return t

def t_BREAK(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value,'BREAK')    # Check for reserved words
    return t

def t_WHILE(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value,'WHILE')    # Check for reserved words
    return t

def t_DO(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value,'DO')    # Check for reserved words
    return t

def t_RETURN(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value,'RETURN')    # Check for reserved words
    return t

# Ignored characters
t_ignore = " \t"

def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("")
    return t

def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)
